package ru.sber.jd.utils;


import ru.sber.jd.dto.Point;

import java.util.ArrayList;
import java.util.List;

public class GenerateUtils {
    public static String newName(String name){
        return "'" + name + "'";
    }



    public static List<Point> generateMapPoint(){
        List<Point> values = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                values.add(Point.builder()
                        .x(i)
                        .y(j)
                        .description("'point:" + i + "-" + j + "'")
                        .build());
            }
        }
        return values;
    }
}
