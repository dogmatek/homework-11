package ru.sber.jd;

import ru.sber.jd.dto.Point;
import ru.sber.jd.utils.GenerateUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Application {
    public static void main(String[] args) {
        List<Point> points = GenerateUtils.generateMapPoint();
        Map<Integer, Map<Integer, String>> collect = points.stream()
                .collect(Collectors.groupingBy(Point::getX,
                            Collectors.toMap(Point::getY, Point::getDescription)
                        )
                );

        System.out.println(collect);
    }
}
